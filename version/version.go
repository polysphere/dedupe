package version

import (
	"fmt"
	"github.com/Masterminds/semver"
	"runtime"
)

var (
	str     string
	version semver.Version
)

func init() {
	v, err := semver.NewVersion(str)
	if err != nil {
		fmt.Println(str)
		fmt.Println(err.Error())
		panic("Version error")
	}
	version = *v
}

func Print() {
	fmt.Printf("dedupe %d.%d.%d",
		version.Major(),
		version.Minor(),
		version.Patch(),
	)
	if version.Prerelease() != "" {
		fmt.Printf("-%s", version.Prerelease())
	}
	fmt.Printf(" %s %s %s %s\n",
		version.Metadata(),
		runtime.Version(),
		runtime.GOOS,
		runtime.GOARCH,
	)
}