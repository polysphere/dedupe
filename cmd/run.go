package cmd

import (
	"dedupe/internal"
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(runCmd)
}

var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run scan",
	Run: func(cmd *cobra.Command, args []string) {
		run()
	},
}

func run() {
	//path := "C:/Users/zero/downloads"
	//path := []string{"K:/"}
	fileTypes := []string{".png", ".jpg", ".dng", ".arw", ".nef", ".gif", ".psd"}
	path := []string{"C:/Users/zero/pictures", "D:/", "K:/", "I:/", "F:/", "G:/", "E:/", "H:/"}

	internal.ScanFiles(path, fileTypes)
}