package internal

import (
	"os"
)

type WorkRequest struct {
	task func(Work)
	args Work
}

type Work struct {
	Path string
	F os.FileInfo
	Err error
}