package internal

import (
	"crypto/sha512"
	"fmt"
	"io"
	"log"
	"os"
)

func Checksum(path string) ([]byte, error) {
	file, err := os.Open(path)
	if err != nil {
		fmt.Println("Error: ", err)
		return nil, err
	}

	defer func() {
		err := file.Close()
		if err != nil {
			fmt.Println("Error: ", err)
		}
		return
	}()

	h := sha512.New()
	if _, err := io.Copy(h, file); err != nil {
		log.Fatal("Log Error ", err)
		return  nil, err
	}
	return h.Sum(nil), nil
}