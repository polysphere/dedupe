package internal

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"github.com/mitchellh/go-homedir"
)

var (
	db *sqlx.DB
)

func Init() {
	fmt.Println("Init SQL")
	db_file, _ := homedir.Expand("~/dedupe.db")
	//_ = os.Remove(db_file)
	//checkErr(err)


	db = sqlx.MustOpen("sqlite3", db_file)
	db.MustExec(`
		CREATE TABLE IF NOT EXISTS file_info(
			path TEXT NOT NULL PRIMARY KEY,
			name TEXT NOT NULL,
			size NUMERIC NOT NULL,
			hex_sum TEXT NOT NULL
		);
	`)
	db.SetMaxOpenConns(1)
}

//func GetDataDupes() *map[string]FileInfo {
//	var duplicates = make(map[string]*FileInfo)
//
//	q, err := dot.Raw("get-data-dupes")
//	stmt, err := db.Preparex(q)
//	checkErr(err)
//	rows, err := stmt.Queryx()
//	checkErr(err)
//
//	for rows.Next() {
//		var fi FileInfo
//		var fd FileDupe
//		err = rows.StructScan(&fi)
//		err = rows.StructScan(&fd)
//
//		checkFiles(duplicates, fi, fd)
//	}
//}

func checkFiles(fileMap map[string]*FileInfo, fileInfo FileInfo, fileDupe FileDupe) {
	//fileInfo := fileMap[fileInfo.Hex_sum]
	//if fileInfo == nil {
	//	fileInfo[data] =
	//}
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func Inzert(file *File) {
	//stmt, err := db.PrepareNamed(`
	//	INSERT INTO file_info(path, name, size, hex_sum) VALUES (:path, :name, :size, :hex_sum)
	//	ON CONFLICT(path) DO NOTHING;
	//`)
	//checkErr(err)
	//stmt.MustExec(file)
	_, err := db.NamedExec(`
		INSERT INTO file_info(path, name, size, hex_sum) VALUES (:path, :name, :size, :hex_sum)
		ON CONFLICT(path) DO NOTHING;
	`, file)
	checkErr(err)
}