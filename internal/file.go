package internal

type FileInfo struct {
	Hex_sum string
	Size int32
	FilesFound []FileDupe
	FilesToDelete []FileDupe
}

type FileDupe struct {
	Name string
	Path string
}

func (fi *FileInfo) append(f FileDupe) {
	fi.FilesFound = append(fi.FilesFound, f)
}

func (fi *FileInfo) delete(file FileDupe) {
	fi.FilesToDelete = append(fi.FilesToDelete, file)
}

type File struct {
	Name string
	Path string
	Hex_sum string
	Size int64
}