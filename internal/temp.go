package internal

import (
	"encoding/hex"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
	"sync"
)

var ext []string
var wg sync.WaitGroup
var WorkQueue = make(chan WorkRequest, 1000)

func walkFunc(path string, f os.FileInfo, err error) error {
	wg.Add(1)
	WorkQueue <- WorkRequest{WorkFunc, Work{path, f, err}}
	return nil
}

func ScanFiles(paths []string, types []string) {
	ext = types
	// Start the dispatcher.
	fmt.Println("Starting the dispatcher")
	StartDispatcher(100)

	Init()

	// Search filesystem
	for _, f := range paths {
		err := filepath.Walk(f, walkFunc)
		if err != nil {
			panic(err)
		}
	}

	// Wait till scan is finished
	wg.Wait()
}

func contains(s string) bool {
	for _, e := range ext {
		if strings.ToLower(e) == strings.ToLower(s) {
			return true
		}
	}
	return false
}

func WorkFunc(w Work) {
	defer func() {
		//if r := recover(); r != nil {
		//	fmt.Println("Recovered in f", r)
		//}
		wg.Done()
	}()

	// TODO: Add as option
	// Limit FileDupe Size
	// Don't do Directories
	e := path.Ext(w.Path)
	if w.F.IsDir() || w.F.Size() > 100000000 || !contains(e) {
		return
	}

	sum, err := Checksum(w.Path)
	if err != nil {
		fmt.Println("Error: ", err)
	}

	Inzert(&File{
		Path: w.Path,
		Name: w.F.Name(),
		Hex_sum: hex.EncodeToString(sum),
		Size: w.F.Size(),
	})

	fmt.Printf("Visited: %s FileDupe name: %s Size: %d bytes Checksum: %s\n", w.Path, w.F.Name(), w.F.Size(), hex.EncodeToString(sum))
}