-- name: create-file-table
CREATE TABLE file_info(
    path TEXT NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    size NUMERIC NOT NULL,
    hex_sum TEXT NOT NULL
);

CREATE INDEX hex_sum_idx ON file_info(hex_sum);

-- Query Duplicate File Checksums
SELECT hex_sum, COUNT(*) AS count
FROM file_info
GROUP BY hex_sum
HAVING COUNT(*) > 1;

SELECT f.*, f2.count
FROM file_info f
INNER JOIN (
    SELECT hex_sum, COUNT(*) AS count
    FROM file_info
    GROUP BY hex_sum
    HAVING COUNT(*) > 1
) f2 ON f.hex_sum = f2.hex_sum;

-- Query Duplicate File Names
SELECT name, COUNT(*) AS count
FROM file_info
GROUP BY hex_sum
HAVING COUNT(*) > 1;

SELECT f.*, f2.count
FROM file_info f
INNER JOIN (
    SELECT name, COUNT(*) AS count
    FROM file_info
    GROUP BY hex_sum
    HAVING COUNT(*) > 1
) f2 ON f.name = f2.name;

-- Delete File
DELETE FROM file_info WHERE path = ?;

-- name: insert-file
INSERT INTO file_info(path, name, size, hex_sum) VALUES (:path, :name, :size, :hex_sum);