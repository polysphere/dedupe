module dedupe

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/Masterminds/semver v1.4.2
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/mitchellh/go-homedir v1.0.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.2.1
	github.com/stretchr/testify v1.3.0 // indirect
	google.golang.org/appengine v1.6.1 // indirect
)
