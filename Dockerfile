FROM karalabe/xgo-latest:latest AS builder

WORKDIR /dedupe

COPY . .

#Build Windows
RUN GOOS=windows GOARCH=amd64 CGO_ENABLED=1 CC=x86_64-w64-mingw32-gcc CXX=x86_64-w64-mingw32-g++ go build -o dedupe.exe -ldflags "-X 'dedupe/version.str=v0.0.0'"

#Build Linux
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=1 go build -o dedupe -ldflags "-X 'dedupe/version.str=v0.0.0'"

#Build Darwin
RUN GOOS=darwin GOARCH=amd64 CGO_ENABLED=1 CC=o64-clang CXX=o64-clang++ go build -o dedupe_darwin -ldflags "-X 'dedupe/version.str=v0.0.0'"
RUN ls -la
